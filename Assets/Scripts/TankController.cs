﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TankController : MonoBehaviour
{
	
	public float maxX = 95;
	public float maxZ = 95;
	public float health = 100;
	public float shotSpeed = 10000;
	public GameObject explosion;
	public GameObject projectile;
	public Material teamAMaterial;
	public Material teamBMaterial;
	public Team team;

	private float directionChangeInterval = 0;
	private new Rigidbody rigidbody;
	private NavMeshAgent navAgent;
	private GameObject marker;
	private GameManager gameManager;
	private AudioSource audio;
	private RaycastHit hit;
	private bool canFire;

	// Use this for initialization
	void Start ()
	{
		rigidbody = GetComponent <Rigidbody> ();
		navAgent = GetComponent <NavMeshAgent> ();
		marker = transform.FindChild ("marker").gameObject;
		gameManager = FindObjectOfType <GameManager> ();
		audio = transform.FindChild ("Tank").GetComponent <AudioSource> ();
		canFire = true;

		Random.seed = System.DateTime.Now.Millisecond;
		directionChangeInterval = Random.Range (1, 10);

		StartCoroutine (NewHeading ());



		// Set marker material based on team
		if (team == Team.A) {
			marker.GetComponent<Renderer> ().material = teamAMaterial;
		} else if (team == Team.B) {
			marker.GetComponent<Renderer> ().material = teamBMaterial;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Raycasting for shooting
		Vector3 fwd = transform.TransformDirection (Vector3.forward); // the direction the tank is facing
		if (Physics.Raycast (transform.position, fwd, 20)) {
			if (canFire) {
				Fire ();
				canFire = !canFire;
			}
		}

		// Destroy tank
		if (health <= 0) {
			Destroy (gameObject);
			if (explosion) {
				Instantiate (explosion, transform.position, transform.rotation);
			}
		}
	}

	void OnDestroy ()
	{
		gameManager.CheckForGameEnd ();
	}

	public void Fire ()
	{
		audio.Play ();
		Transform missileExit = transform.FindChild ("Tank").FindChild ("Missile Exit");
		GameObject clone = (GameObject)Instantiate (projectile, missileExit.position, missileExit.rotation);
		clone.GetComponent<Rigidbody> ().AddForce (transform.forward * shotSpeed);
	}

	void OnCollisionEnter (Collision collision)
	{
		Debug.Log (GetComponent<Collider> ().name);
		Collider collider = collision.collider;
		//if (collider.gameObject.name == "Missile") {
		Debug.Log ("ATTACKED!");
		health -= collider.transform.GetComponent<MissileController> ().damage;
		Debug.Log ("New health: " + health);
		//}
	}

	/// <summary>
	/// Repeatedly calculates a new direction to move towards.
	/// Use this instead of MonoBehaviour.InvokeRepeating so that the interval can be changed at runtime.
	/// </summary>
	IEnumerator NewHeading ()
	{
		while (true) {
			ChangeDirectionRoutine ();
			yield return new WaitForSeconds (directionChangeInterval);
		}
	}

	/// <summary>
	/// Changes direction to move towards.
	/// </summary>
	void ChangeDirectionRoutine ()
	{
		if (navAgent) {
			Random.seed = System.DateTime.Now.Millisecond * Random.Range (0, 1000);
			float xVal = Random.Range (5, maxX);
			float zVal = Random.Range (5, maxZ);
			Vector3 newDest = new Vector3 (xVal, 0, zVal);
			navAgent.SetDestination (newDest);
		} else {
			Debug.Log ("No NavAgent");
		}
		if (!canFire) {
			canFire = !canFire;
		}
	}
}