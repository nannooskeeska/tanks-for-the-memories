﻿using UnityEngine;
using System.Collections;

public class MissileController : MonoBehaviour
{

	public GameObject explosion;
	public float damage = 20;

	// Use this for initialization
	void Start ()
	{
		// Destroy the missile after 3 seconds to avoid clutter
		Destroy (gameObject, 3);
	}

	void OnCollisionEnter ()
	{
		Destroy (gameObject);
		Instantiate (explosion, transform.position, transform.rotation);
	}
}