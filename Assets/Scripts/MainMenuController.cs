﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{

	public static bool isDark = true;
	public static float numEnemies;
	public static float numFriends;

	private Slider enemiesSlider;
	private Slider friendsSlider;

	// Use this for initialization
	void Start ()
	{
		numEnemies = 1;
	}

	// Sets numEnemies
	public void OnEnemyValueChanged (Slider slider)
	{
		numEnemies = slider.value;
		slider.GetComponentsInChildren<Text> () [1].text = slider.value.ToString ();
		//Debug.Log (numEnemies);
	}

	// Sets numFriends
	public void OnFriendValueChanged (Slider slider)
	{
		numFriends = slider.value;
		slider.GetComponentsInChildren<Text> () [1].text = slider.value.ToString ();
		//Debug.Log (numFriends);
	}

	// Sets isDark
	public void OnDarkToggleChanged (Toggle toggle)
	{
		isDark = toggle.isOn;
		Debug.Log (isDark);
	}

	// Opens game
	public void OpenGame ()
	{
		SceneManager.LoadScene ("Main");
	}
}
