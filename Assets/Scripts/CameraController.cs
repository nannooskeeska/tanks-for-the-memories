﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public Transform cameraTarget;
	public float distanceAbove = 0.5f;
	public float distanceBehind = 10;

	private RaycastHit hit;

	// Use this for initialization
	void Start ()
	{
		cameraTarget.GetComponent <PlayerController> ().enabled = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 camPos = cameraTarget.position - (cameraTarget.forward * distanceBehind) + (Vector3.up * distanceAbove);
		transform.position = camPos;
		transform.LookAt (cameraTarget);
	}
}