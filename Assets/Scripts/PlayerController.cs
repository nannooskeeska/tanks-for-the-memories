﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	//TODO: move with WASD and look with mouse

	public float speed = 10;
	public float rotationSpeed = 10;
	public GameObject projectile;
	public float shotSpeed;
	public float timeBetweenShots = 0.25f;
	public float health = 140;

	private float timeStamp;
	private new Rigidbody rigidbody;
	private GameManager gameManager;
	private AudioSource audio;

	// Use this for initialization
	void Start ()
	{
		rigidbody = GetComponent <Rigidbody> ();
		audio = transform.FindChild ("Tank").GetComponent <AudioSource> ();
	}

	void Update ()
	{
		// Fire primary weapon
		if (Time.time >= timeStamp && Input.GetButtonDown ("Fire1")) {
			if (projectile) {
				Fire ();
				timeStamp = Time.time + timeBetweenShots;
			} else {
				Debug.LogError ("Please set a projectile to be fired.");
			}
		}

		// Toggle light on and off
		if (Input.GetButtonDown ("Fire2")) {
			ToggleLight ();
		}

		// Destroy player and load main menu
		// TODO: game over screen first
		if (health <= 0) {
			Destroy (gameObject);
			SceneManager.LoadScene ("Game Over Loss");
		}
	}

	void FixedUpdate ()
	{
		float forwardForce = Input.GetAxis ("Vertical");
		float rotationForce = Input.GetAxis ("Horizontal");

		// Forward
		rigidbody.velocity = transform.forward * speed * forwardForce;

		// Rotate
		rigidbody.AddTorque (0, rotationForce * rotationSpeed, 0);
	}

	void OnCollisionEnter (Collision collision)
	{
		Collider collider = collision.collider;
		if (collider.gameObject.name == "Missile") {
			health -= collider.transform.GetComponent<MissileController> ().damage;
		}
	}

	// Fire the cannon
	void Fire ()
	{
		audio.Play ();
		Transform missileExit = transform.FindChild ("Tank").FindChild ("Missile Exit");
		GameObject clone = (GameObject)Instantiate (projectile, missileExit.position, missileExit.rotation);
		clone.GetComponent<Rigidbody> ().AddForce (transform.forward * shotSpeed);
	}

	// Toggle the headlight
	void ToggleLight ()
	{
		GameObject headlight = transform.FindChild ("Headlight").gameObject;
		if (headlight.activeSelf) {
			headlight.SetActive (false);
		} else {
			headlight.SetActive (true);
		}
	}
}
