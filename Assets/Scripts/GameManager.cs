﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Linq;

public class GameManager : MonoBehaviour
{

	public GameObject tank;

	private bool isDark;
	private int botsOnTeamA = 0;
	private int botsOnTeamB = 1;
	private GameObject[] enemyTanks;
	private GameObject[] friendlyTanks;
	private GameObject[] spawners;

	// Use this for initialization
	void Start ()
	{
		isDark = MainMenuController.isDark;
		Debug.Log (isDark);
		GameObject.Find ("Directional Light").SetActive (!isDark);

		botsOnTeamA = (int)MainMenuController.numFriends;
		botsOnTeamB = (int)MainMenuController.numEnemies;

		// Set spawners array and sort it by name
		spawners = GameObject.FindGameObjectsWithTag ("Spawner").OrderBy (go => go.name).ToArray ();

		// Initialize tank arrays
		enemyTanks = new GameObject[botsOnTeamB];
		friendlyTanks = new GameObject[botsOnTeamA];

		// Spawn friendly tanks
		for (int i = 0; i < friendlyTanks.Length; i++) {
			GameObject curSpawn = spawners [i];
			friendlyTanks [i] = (GameObject)Instantiate (tank, curSpawn.transform.position, curSpawn.transform.rotation);
		}

		// Spawn enemy tanks
		for (int i = 4; i < enemyTanks.Length + 4; i++) {
			GameObject curSpawn = spawners [i];
			enemyTanks [i - 4] = (GameObject)Instantiate (tank, curSpawn.transform.position, curSpawn.transform.rotation);
		}

		// Set the teams for the tanks
		foreach (GameObject tank in friendlyTanks) {
			tank.GetComponent<TankController> ().team = Team.A;
		}

		foreach (GameObject tank in enemyTanks) {
			tank.GetComponent<TankController> ().team = Team.B;
		}
	}

	int NotNullValues (GameObject[] arr)
	{
		int num = 0;
		foreach (GameObject obj in arr) {
			if (obj) {
				num++;
			}
		}
		return num;
	}

	public void CheckForGameEnd ()
	{
		if (NotNullValues (enemyTanks) == 1) {
			Debug.Log ("Player should win");
			SceneManager.LoadScene ("Game Over Win");
		}
	}
}
